package model;

import config.SimulatorConfig;

public class Vorstadtsiedlung
{
    //TODO:
    /*
     * UpdateLandschaft, Methode um die Dimension der Landschaft zu verändern
     * Platzieren von Barrieren und Tiere und Essen
     *
     */
    private int anzahlKacheln_Y = 10;
    private int anzahlKacheln_X = 10;
    private int[][] kacheln;

    public Vorstadtsiedlung()
    {
        this.initGame();
    }

    /**
     * Stellt die fuer das Spiel benoetigten Ressourcen zur verfuegung.
     */
    private void initGame()
    {
        this.kacheln = new int[this.anzahlKacheln_X][this.anzahlKacheln_Y];
        initVorstadtsiedlung();
    }

    /**
     * Methode um die Kacheln zu beginn mit Nullen zu befüllen.
     */
    private void initVorstadtsiedlung()
    {
        for(int i = 0; i<this.anzahlKacheln_X; i++)
        {
            for(int j = 0; j<this.anzahlKacheln_Y; j++)
            {
                    this.kacheln[i][j] = SimulatorConfig.EMPTY_VALUE;
            }
        }
        this.kacheln[0][1] = SimulatorConfig.BARRIERE_VALUE;
        this.kacheln[1][0] = SimulatorConfig.ESSEN_VALUE;
        this.kacheln[1][1] = SimulatorConfig.TIER_VALUE;
        this.kacheln[0][2] = SimulatorConfig.BARRIERE_VALUE;
        this.kacheln[2][0] = SimulatorConfig.BARRIERE_VALUE;
        this.kacheln[8][4] = SimulatorConfig.BARRIERE_VALUE;
        this.kacheln[6][2] = SimulatorConfig.TIER_VALUE;
        this.kacheln[4][8] = SimulatorConfig.ESSEN_VALUE;

    }


    public void printVorstadt(Waschbaer willy)
    {
        for(int i = 0; i<this.anzahlKacheln_X; i++)
        {
            for(int j = 0; j<this.anzahlKacheln_Y; j++)
            {
                if(willy.getWaschbaerPosition_X() == j && willy.getWaschbaerPosition_Y() == i)
                {
                    System.out.print("  " + SimulatorConfig.WASCHBAER_VALUE);
                }
                else {
                    System.out.print("  " + this.kacheln[i][j]);
                }
            }
            System.out.println();
        }
    }


    /**
     *
     * Legt einen Gegenstand auf die angegebene Koordinate.
     *
     * @param x X-Koordinate auf die der Gegenstand abgelegt wird.
     * @param y Y-Koordinate auf die der Gegenstand abgelegt wird.
     */
    public void legeGegenstandAufKachel(int x, int y)
    {
        if(this.kacheln[y][x] == SimulatorConfig.ESSEN_VALUE)
        {
            throw new RuntimeException("Hier liegt bereits was zu essen.");
        }
        if(this.kacheln[y][x] == SimulatorConfig.TIER_VALUE)
        {
            //UEBERGEBE DEM  TIER DAS ESSEN UND ENTFERNE ES DADURCH
            this.kacheln[y][x] = SimulatorConfig.EMPTY_VALUE;
        }
        else {
            this.kacheln[y][x] = SimulatorConfig.ESSEN_VALUE;
        }
    }


    /**
     *
     * Methode dient dazu eine Barriere in der Landschaft zu platzieren.
     *
     * @param x
     * @param y
     */
    public void platziereBarriere(int x, int y)
    {
        if(this.kacheln[y][x] == SimulatorConfig.EMPTY_VALUE)
        {
            this.kacheln[y][x] = SimulatorConfig.BARRIERE_VALUE;
        }
    }

    /**
     *
     * Gibt den aktuellen Gegenstands-Index zurueck.
     *
     * @param x die X-Koordinate
     * @param y die y-Koordinate
     * @return den Index des Gegenstands.
     */
    public int nimmWertVonKoord(int x, int y)
    {
        int returnValue = this.kacheln[y][x];
        return returnValue;
    }

    public int getAnzahlKacheln_Y() {
        return anzahlKacheln_Y;
    }

    public int getAnzahlKacheln_X() {
        return anzahlKacheln_X;
    }
}
