package model;

import config.SimulatorConfig;

import java.util.ArrayList;

/**
 *
 * Dies ist Modell des Waschbaeren, des Akteurs der Simulation.
 * Es werden verschiedene Statuswerte bereitgestellt und die Methoden zum
 * aufsammeln von Gegenstaenden.
 *
 * @author Christian Blank
 * @version 1.0
 *
 */
public class Waschbaer
{
    /**
     * Position des Waschbears in der Landschaft
     */
    private int waschbaerPosition_Y;
    private int waschbaerPosition_X;

    /**
     * Diese Variable repraesentiert die Blickrichtung des Waschbaers.
     * 1 = Norden
     * 0 = Osten
     * 3 = Sueden
     * 2 = Westen
     *
     */
    private int waschbaerBlickRichtung;


    /**
     * Status ob der Waschbaer freie Pfoten hat oder nicht.
     */
    private boolean waschbaerPfoteFrei = true;

    /**
     * Dies ist die Landschaft in der der Waschbaer lebt.
     * Das dieser die Landschaft manipulieren kann, muss er diese auch kennen.
     */
    private Vorstadtsiedlung vorstadtsiedlung;

    /**
     *
     * Der Konstruktor setzt den Waschbaer initial auf seine Startposition.
     * Jeder Waschbaer startet immer in der oberen linken Ecke.
     *
     */
    public Waschbaer(Vorstadtsiedlung vorstadtsiedlung)
    {
        this.vorstadtsiedlung = vorstadtsiedlung;
        this.waschbaerPosition_X = 0;
        this.waschbaerPosition_Y = 0;
        this.waschbaerBlickRichtung = 0;
    }




    public int getWaschbaerPosition_Y() {
        return waschbaerPosition_Y;
    }
    public int getWaschbaerPosition_X() {
        return waschbaerPosition_X;
    }

    public String getWaschbaerBlickRichtungValue() {
        if(this.waschbaerBlickRichtung == 0)
        {
            return "Osten";
        }
        else if(this.waschbaerBlickRichtung == 1)
        {
            return "Norden";
        }
        else if(this.waschbaerBlickRichtung == 2)
        {
            return "Westen";
        }
        else if(this.waschbaerBlickRichtung == 3)
        {
            return "Süden";
        }
        return null;
    }

    /**
     * Methode um einen Gegenstand aufzuheben.
     * Falls waschbaerPfoteFrei == false wird zuerst die Methode
     * gib() aufgerufen.
     *
     */
    public void aufheben()
    {
        if(this.waschbaerPfoteFrei)
        {
            int value = this.vorstadtsiedlung.nimmWertVonKoord(this.waschbaerPosition_X,this.waschbaerPosition_Y);
            if(value == SimulatorConfig.ESSEN_VALUE)
            {
                this.waschbaerPfoteFrei = false;
                return;
            }
            throw new RuntimeException("Kein Gegenstand gefunden.");
        }
    }

    /**
     *
     * Legt den Gegenstand welchen der Waschbaer aktuell in der Pfote hat ab,
     * sollte in der naechsten Kacheln in Blickrichtung des Waschbaeren ein Tier stehen wird
     * der Gegenstand uebergeben. Ansonsten wird der Gegenstand auf der Aktuellen Kachel abgelegt.
     *
     * Wirft eine Exception wenn der Waschbaer nichts in den Pfoten hat.
     *
     */
    public void gib()
    {
        if(this.waschbaerPfoteFrei)
        {
           throw new RuntimeException("Waschbaer hat nichts in der Hand");
        }
        if(this.vorstadtsiedlung.nimmWertVonKoord(this.naechsteKachel().get(0),this.naechsteKachel().get(1)) == SimulatorConfig.TIER_VALUE)
        {
            this.vorstadtsiedlung.legeGegenstandAufKachel(this.naechsteKachel().get(0),this.naechsteKachel().get(1));
        }
        else{
            this.vorstadtsiedlung.legeGegenstandAufKachel(this.waschbaerPosition_X,this.waschbaerPosition_Y);
        }
        this.waschbaerPfoteFrei = true;
    }


    /**
     * Geht eine Kachel weiter in die Blickrichtung in die der Waschbaer guckt.
     */
    public void vor()
    {
        ArrayList<Integer> naechsteKachel = this.naechsteKachel();
        if(this.wegFrei())
        {
            this.waschbaerPosition_X = naechsteKachel.get(0);
            this.waschbaerPosition_Y = naechsteKachel.get(1);
            return;
        }
        throw new RuntimeException("Der Waschbär kann hier nicht hin.");
    }


    /**
     * Diese Methode aendert die Blickrichtung des Waschbaeren um 90 Grad nach links.
     * Somit gegen den Uhrzeigersinn.
     */
    public void linksDrehen()
    {
        this.waschbaerBlickRichtung = (this.waschbaerBlickRichtung+1)%4;
    }

    /**
     *
     * Prueft ob die Kachel die durch die angegebenen x und y Koordinate
     * definiert ist frei (value=0) ist.
     *
     * @return ob die Kachel frei ist
     */
    public boolean wegFrei()
    {
        try
        {
            ArrayList<Integer> naechsteKachel = this.naechsteKachel();
            if(this.vorstadtsiedlung.nimmWertVonKoord(naechsteKachel.get(0),naechsteKachel.get(1))==SimulatorConfig.EMPTY_VALUE || this.vorstadtsiedlung.nimmWertVonKoord(naechsteKachel.get(0),naechsteKachel.get(1))==SimulatorConfig.ESSEN_VALUE)
            {
                System.out.println("Der Weg ist frei.");
                return true;
            }
        }
        catch(IndexOutOfBoundsException e)
        {
            throw new RuntimeException("Der Waschbaer kann die Siedlung nicht verlassen.");
        }
        System.out.println("Hier kann der Waschbaer nicht lang.");
        return false;
    }


    /**
     *
     * Gibt anhand der Blickrichtung die naechste Kachel zurueck welche der Waschbaer betreten wuerde.
     *
     * @return ArrayList<Integer> mit den x und y Koordinaten der naechsten Kachel.
     */
    private ArrayList<Integer> naechsteKachel()
    {
        ArrayList<Integer> naechsteKachel = new ArrayList<>();
        switch(this.waschbaerBlickRichtung)
        {
            //OST
            case 0:
                naechsteKachel.add(this.waschbaerPosition_X+1);
                naechsteKachel.add(this.waschbaerPosition_Y);
                break;
            //NORD
            case 1:
                naechsteKachel.add(this.waschbaerPosition_X);
                naechsteKachel.add(this.waschbaerPosition_Y-1);
                break;
            //WEST
            case 2:
                naechsteKachel.add(this.waschbaerPosition_X-1);
                naechsteKachel.add(this.waschbaerPosition_Y);
                break;
            //SUED
            case 3:
                naechsteKachel.add(this.waschbaerPosition_X);
                naechsteKachel.add(this.waschbaerPosition_Y+1);
                break;
            default:
                throw new RuntimeException("Der Waschbaer ist verwirrt.");
        }
        return naechsteKachel;
    }
}
