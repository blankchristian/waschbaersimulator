package config;

/**
 *
 * Konfigurations-Klasse. Enthaelt statische Konstanten.
 *
 * @author Christian Blank
 *
 */
public class SimulatorConfig
{
    public static final int BARRIERE_VALUE = -2;
    public static final int TIER_VALUE = 1;
    public static final int ESSEN_VALUE = 2;
    public static final int WASCHBAER_VALUE = 8;
    public static final int EMPTY_VALUE = 0;
    public static final int KACHEL_WIDTH = 35;
    public static final int KACHEL_HEIGHT = 35;
}
