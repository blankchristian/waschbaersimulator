/*
package controller;

import javafx.fxml.FXML;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import model.Vorstadtsiedlung;

public class SimulatorControllerOLD
{
    @FXML
    private TerritoriumPanel drawableCanvas;

    @FXML
    private StackPane drawableStackpane;

    public SimulatorControllerOLD()

    {

    }

    public void createGrid(Vorstadtsiedlung vorstadtsiedlung){
        int width = vorstadtsiedlung.getAnzahlKacheln_X()*35;
        int height = vorstadtsiedlung.getAnzahlKacheln_Y()*35;
        this.drawableCanvas.setWidth(width);
        this.drawableCanvas.setHeight(height);
        GraphicsContext gc = drawableCanvas.getGraphicsContext2D();
        drawableCanvas.autosize();
        for(int i = 0; i <= vorstadtsiedlung.getAnzahlKacheln_X(); i++)
        {
            for(int j = 0; j <= vorstadtsiedlung.getAnzahlKacheln_Y(); j++)
            {
                gc.setFill(Color.GREENYELLOW);
                gc.fillRect((j*35), (i*35), 35, 35);
                gc.strokeRect((j*35), (i*35), 35, 35);
            }
        }
        drawRacoon();
    }

    public void drawRacoon()
    {
        GraphicsContext gc = this.drawableCanvas.getGraphicsContext2D();
        gc.drawImage(new Image("/resources/raccoon.png"),35,35,35,35);

        ImageView racoon = new ImageView();
        racoon.setImage(new Image("/resources/raccoon.png"));
        racoon.setFitHeight(35);
        racoon.setFitWidth(35);
        racoon.setX(0);
        racoon.setY(0);
        this.drawableStackpane.getChildren().add(racoon);
    }
}
*/
