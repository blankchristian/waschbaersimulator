package controller;

import config.SimulatorConfig;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import libs.IO;
import model.Vorstadtsiedlung;
import model.Waschbaer;

public class SimulatorController extends Task<Integer>
{
    @FXML
    private TerritoriumPanel drawableCanvas;
    @FXML
    private StackPane drawableStackpane;
    private GridPane gridPane;


    public SimulatorController()
    {

    }

    @Override
    protected Integer call() throws Exception {
        return 0;
    }

    /**
     * Initiale Methode zum erstellen des Grids.
     * @param vorstadtsiedlung
     */
    public void createGrid(Vorstadtsiedlung vorstadtsiedlung, Waschbaer willy)
    {
        this.drawGridLayout(vorstadtsiedlung);
        this.generateGridLayout();
        this.setRacoonOnMap(0,0);
        //this.game(vorstadtsiedlung, willy);
    }


    private void game(Vorstadtsiedlung vorstadtsiedlung, Waschbaer willy)
    {
        System.out.println("Der Waschbär kommt aus seinem Lager und schaut nach Osten.");
        while(true)
        {
            System.out.println("Was soll der Waschbär machen:");
            char input = IO.readChar();
            switch(input)
            {
                case 'v':
                    try
                    {
                        willy.vor();
                        this.setRacoonOnMap(willy.getWaschbaerPosition_X(), willy.getWaschbaerPosition_Y());
                    }
                    catch(Exception e)
                    {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 'd':
                    try
                    {
                        willy.linksDrehen();
                    }
                    catch(Exception e)
                    {
                        System.out.println(e.getMessage());
                    }
                    System.out.println("Der Waschbaer guckt nach "+willy.getWaschbaerBlickRichtungValue());
                    break;
                case 'a':
                    try
                    {
                        willy.aufheben();
                    }
                    catch(Exception e)
                    {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 'g':
                    try{
                        willy.gib();
                    }
                    catch(Exception e)
                    {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 'f':
                    try
                    {
                        willy.wegFrei();
                    }
                    catch(Exception e)
                    {
                        System.out.println(e.getMessage());
                    }
                    break;
            }
            System.out.println();
            vorstadtsiedlung.printVorstadt(willy);
        }
    }

    /**
     *
     * Zeichnet die Grid-Darstellung in das Canvas
     *
     * @param vorstadtsiedlung
     */
    private void drawGridLayout(Vorstadtsiedlung vorstadtsiedlung)
    {
        drawableCanvas.setHeight(35*vorstadtsiedlung.getAnzahlKacheln_Y());
        drawableCanvas.setWidth(35*vorstadtsiedlung.getAnzahlKacheln_Y());
        GraphicsContext gc = drawableCanvas.getGraphicsContext2D();
        for (int i = 0; i <= vorstadtsiedlung.getAnzahlKacheln_X(); i++) {
            for (int j = 0; j <= vorstadtsiedlung.getAnzahlKacheln_Y(); j++) {
                gc.setFill(Color.GREENYELLOW);
                gc.fillRect((j * SimulatorConfig.KACHEL_HEIGHT), (i * SimulatorConfig.KACHEL_WIDTH), SimulatorConfig.KACHEL_WIDTH, SimulatorConfig.KACHEL_HEIGHT);
                gc.strokeRect((j * SimulatorConfig.KACHEL_HEIGHT), (i * SimulatorConfig.KACHEL_WIDTH), SimulatorConfig.KACHEL_WIDTH, SimulatorConfig.KACHEL_HEIGHT);
            }
        }

    }

    /**
     * Erzeugt das Grid-Layout welches zur platzierung der Elemente in der Landschaft dient.
     */
    private void generateGridLayout()
    {
        this.gridPane = new GridPane();
        this.drawableStackpane.getChildren().add(this.gridPane);
        RowConstraints rowConstraints = new RowConstraints(SimulatorConfig.KACHEL_HEIGHT);
        ColumnConstraints columnConstraints = new ColumnConstraints(SimulatorConfig.KACHEL_WIDTH);
        for(int i = 0; i<10;i++)
        {
            this.gridPane.getRowConstraints().add(rowConstraints);
            this.gridPane.getColumnConstraints().add(columnConstraints);
        }
    }

    /**
     * Methode zum platzieren des Waschbaers auf dem Grid
     * @param x
     * @param y
     */
    private void setRacoonOnMap(int x, int y)
    {
        ImageView racoon = new ImageView("/resources/raccoon.png");
        racoon.setFitWidth(SimulatorConfig.KACHEL_WIDTH);
        racoon.setFitHeight(SimulatorConfig.KACHEL_HEIGHT);
        this.gridPane.add(racoon,y,x);
    }
}
