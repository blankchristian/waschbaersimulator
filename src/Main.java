import controller.SimulatorController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.Vorstadtsiedlung;
import model.Waschbaer;


public class Main extends Application
{
    public static void main(String [] args)
    {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        Vorstadtsiedlung vorstadtsiedlung = new Vorstadtsiedlung();
        Waschbaer willy = new Waschbaer(vorstadtsiedlung);
        vorstadtsiedlung.printVorstadt(willy);

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("view/GUI.fxml"));
        Parent root = fxmlLoader.load();
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();

        SimulatorController simulatorController = fxmlLoader.getController();
        simulatorController.createGrid(vorstadtsiedlung, willy);

    }
}